/**
* Created with a5.
* User: nicksegal
* Date: 2014-11-17
* Time: 03:40 AM
* To change this template use Tools | Templates.
* Description: To set the navbar active links obviously :P
*/


$(function () {
    setNavigation();    
});

function setNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = path.replace(/\//, "");
    path = decodeURIComponent(path);
    

    $(".nav a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('active');
        }
    });
}
